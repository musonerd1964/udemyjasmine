module.exports = function(config) {
	config.set({
		frameworks: ['jasmine', 'jasmine-matchers', 'jquery-3.4.0'],
		preprocessors: {
			'js/*.js': ['coverage']
		},
		files: [
			'js/custom-matchers.js',
			'js/*.js',
			'js/*.spec.js'
		],
		plugins: [
			'karma-jasmine',
			'karma-jasmine-matchers',
			'karma-jquery',
			'karma-chrome-launcher',
			'karma-coverage'
		],
		reporters: ['dots', 'coverage'],
		color: true,
		browsers: ['ChromeHeadless'],
		singleRun: true,
		coverageReporter: {
			dir: 'coverage/',
			reporters: [
				{
					type: 'html',
					subdir: 'html'
				}
			]
		}
	});
}