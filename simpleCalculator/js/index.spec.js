describe('main.js', function() {
	describe('calculate()', function() {
		it('Validates expression when first number is invalid', function() {
			spyOn(window, 'updateResult').and.stub();
			calculate('a+3');
			expect(window.updateResult).toHaveBeenCalled();
			expect(window.updateResult).toHaveBeenCalledWith('Invalid expression');
			expect(window.updateResult).toHaveBeenCalledTimes(1);
		});
		it('Validates expression when second number is invalid', function() {
			spyOn(window, 'updateResult').and.stub();
			calculate('3+a');
			expect(window.updateResult).toHaveBeenCalled();
			expect(window.updateResult).toHaveBeenCalledWith('Invalid expression');
			expect(window.updateResult).toHaveBeenCalledTimes(1);
		});
		it('Validates expression when operator is invalid', function() {
			spyOn(window, 'updateResult').and.stub();
			calculate('3_4');
			expect(window.updateResult).toHaveBeenCalled();
			expect(window.updateResult).toHaveBeenCalledWith('Invalid expression');
			expect(window.updateResult).toHaveBeenCalledTimes(1);
		});
		it('Calls add()', function() {
			const spy = spyOn(SimpleCalculator.prototype, 'add');
			calculate('3+4');
			expect(spy).toHaveBeenCalledTimes(2);
			expect(spy).toHaveBeenCalledWith(3);
			expect(spy).toHaveBeenCalledWith(4);
		});
		it('Calls subtract()', function() {
			const spy = spyOn(SimpleCalculator.prototype, 'subtract');
			const spy1 = spyOn(SimpleCalculator.prototype, 'add');
			calculate('4-3');
			expect(spy).toHaveBeenCalledTimes(1);
			expect(spy1).toHaveBeenCalledTimes(1);
			expect(spy1).toHaveBeenCalledTimes(1);
			expect(spy).toHaveBeenCalledWith(3);
			expect(spy1).toHaveBeenCalledWith(4);
		});
		it('Calls multiply()', function() {
			const spy = spyOn(SimpleCalculator.prototype, 'multiply');
			const spy1 = spyOn(SimpleCalculator.prototype, 'add');
			calculate('3*4');
			expect(spy).toHaveBeenCalledTimes(1);
			expect(spy1).toHaveBeenCalledTimes(1);
			expect(spy).toHaveBeenCalledWith(4);
			expect(spy).not.toHaveBeenCalledWith(3);
			expect(spy1).toHaveBeenCalledWith(3);
		});
		it('Calls divide()', function() {
			const spy = spyOn(SimpleCalculator.prototype, 'divide');
			const spy1 = spyOn(SimpleCalculator.prototype, 'add');
			calculate('3/4');
			expect(spy).toHaveBeenCalledTimes(1);
			expect(spy1).toHaveBeenCalledTimes(1);
			expect(spy).toHaveBeenCalledWith(4);
			expect(spy).not.toHaveBeenCalledWith(3);
			expect(spy1).toHaveBeenCalledWith(3);
		});
		it('Calls updateResult (example using and.callThrough)', function() {
			spyOn(window, 'updateResult');
			spyOn(SimpleCalculator.prototype, 'multiply').and.callThrough();
			calculate('5*5');
			expect(window.updateResult).toHaveBeenCalled();
			expect(window.updateResult).toHaveBeenCalledWith(25);
		});
		it('Calls updateResult (example using and.callFake)', function() {
			spyOn(window, 'updateResult');
			spyOn(SimpleCalculator.prototype, 'multiply').and.callFake(function(number) {
				return 'it works';
			});
			calculate('5*5');
			expect(window.updateResult).toHaveBeenCalled();
			expect(window.updateResult).toHaveBeenCalledWith('it works');
		});
		it('Calls updateResult (example using and.returnValue)', function() {
			spyOn(window, 'updateResult');
			spyOn(SimpleCalculator.prototype, 'multiply').and.returnValue('whatever [multiply] return');
			calculate('5*5');
			expect(window.updateResult).toHaveBeenCalled();
			expect(window.updateResult).toHaveBeenCalledWith('whatever [multiply] return');
		});
		it('Calls updateResult (example using and.returnValues)', function() {
			spyOn(window, 'updateResult');
			spyOn(SimpleCalculator.prototype, 'add').and.returnValues(null, 'whatever [add] returns');
			calculate('5+5');
			expect(window.updateResult).toHaveBeenCalled();
			expect(window.updateResult).toHaveBeenCalledWith('whatever [add] returns');
		});
		it('Does not handle errors', function () {
			spyOn(SimpleCalculator.prototype, 'multiply').and.throwError('Some error');
			expect(function () {calculate('5*5')}).toThrowError('Some error');
		});
	});

	describe('updateResult()', function() {
		beforeAll(function() {
			this.element = $('<div>').attr({'id': 'result', 'class': 'myClass'});
			$('body').append(this.element);
		});

		afterAll(function() {
			$('body div.myClass').remove();
		});

		it('Adds result to the DOM element', function() {
			updateResult('5');
			expect(this.element.text()).toBe('5');
		});
	});

	describe('showVersion()', function() {
		it('Calls calculator.version', function(done) {
			spyOn(document, 'getElementById').and.returnValue({innerText: null});
			let spy = spyOnProperty(SimpleCalculator.prototype, 'version', 'get').and.returnValue(
				Promise.resolve()
			);
			showVersion();
			expect(spy).toHaveBeenCalled();
			done();
		});
	});
});