function calculate (inputValue) {
	const expression = /\+|\-|\*|\//;
	const numbers = inputValue.split(expression);
	const numberA = Number(numbers[0]);
	const numberB = Number(numbers[1]);
	const operator = inputValue.match(expression);
	let result;

	if (Number.isNaN(numberA) || Number.isNaN(numberB) || operator === null) {
		result = 'Invalid expression';
		updateResult(result);
		return;
	}
	const calculator = new SimpleCalculator();
	calculator.add(numberA);

	switch(operator[0]) {
		case '+':
			result = calculator.add(numberB);
			break;
		case '-':
			result = calculator.subtract(numberB);
			break;
		case '*':
			result = calculator.multiply(numberB);
			break;
		case '/':
			result = calculator.divide(numberB);
			break;
	}
	updateResult(result);
	showVersion();
}

function updateResult(result) {
	const resultSpan = $('#result');
	if (resultSpan.length) {
		resultSpan.text(result);
	}
}

function showVersion() {
	const calculator = new SimpleCalculator();
	calculator.version
		.then(function(version) {
			$('#version').text(version);
		})		
		.catch(function(error) {
			$('#version').text('unknown');
		})
}

$(document).ready(
    function() {
		$ ('#inputExpression').change(
			function() {
				calculate($(this).val());
			}
		);
    }
);
