describe('simpleCalculator.js', function() {
	describe('SimpleCalculator', function() {
		let calculator1;
		let calculator2;

		beforeEach(function() {
			calculator1 = new SimpleCalculator();
			calculator2 = new SimpleCalculator();
		});

		afterEach(function() {
			//
		});

		xit('Should have a constructor', function() {
			expect(calculator1).toEqual(calculator1);
		});

		it('Can be instantiated', function() {
			jasmine.addMatchers(customMatchers);
			expect(calculator2).toBeCalculator();
			expect(calculator1).toBeTruthy();
			expect(calculator1.total).toBeFalsy();
		});

		it('Instantiates unique object', function() {
			expect(calculator1).not.toBe(calculator2);
			expect(calculator1.constructor.name).toContain('Calc');
		});

		it('Has common operations', function() {
			expect(calculator1.add).toBeDefined();
			expect(calculator1.subtract).toBeDefined();
			expect(calculator1.multiply).not.toBeUndefined();
			expect(calculator1.divide).not.toBeUndefined();
		});

		it('Should initialise total', function() {
			expect(calculator1.total).toBe(0);
		});

		describe('add()', function() {
			it('Should add number to the total', function() {
				calculator1.add(5);
				expect(calculator1.total).toBe(5);
			});

			it('Returns Total', function() {
				calculator1.total = 70;
				expect(calculator1.add(20)).toBe(90);
				expect(calculator1.total).toMatch(/-?\d/);
				expect(calculator1.total).toBeNumber();
			});
		});

		describe('subtract()', function() {
			it('Should subtract number from total', function() {
				calculator1.subtract(5);
				expect(calculator1.total).toBe(-5);
			});
		});

		describe('multiply()', function() {
			it('Doesn\'t Handle NaN', function() {
				calculator1.total = 20;
				calculator1.multiply('a')
				expect(calculator1.total).toBeNaN(0);
			});

			it('Should multiply total by number', function() {
				calculator1.total = 5;
				calculator1.multiply(5);
				expect(calculator1.total).toBe(25);
			});
		});

		describe('get version', function() {
			it('fetches version from external source', async function(done) {
				spyOn(window, 'fetch').and.returnValue(Promise.resolve(
					new Response('{"version": "0.1"}')
					));
				const version = await calculator1.version;
				expect(version).toBe('0.1');
				done();
			});
		});

		describe('divide()', function() {
			it('Should divide total by number', function() {
				calculator1.total = 5;
				calculator1.divide(5);
				expect(calculator1.total).toBe(1);
			});

			it('Throws Error on divide by Zero', function() {
				calculator1.total = 20;
				expect(function() {
					calculator1.divide(0);
				}).toThrow();
				expect(function() {
					calculator1.divide(0);
				}).toThrowError(Error, 'Cannot divide by 0');
			});
		});
	});
});