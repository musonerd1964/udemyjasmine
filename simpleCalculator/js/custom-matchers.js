const customMatchers = {
	toBeCalculator: function() {
		return {
			compare: function(actual) {
				const result = {
					pass: actual instanceof SimpleCalculator,
					message: ''
				}

				if (result.pass) {
					result.message = actual + ' is an instance of simpleCalculator';
				} else {
					result.message = 'Expected ' + actual + ' to be an instance of simpleCalculator';
				}

				return result;
			}
		}

	}
}