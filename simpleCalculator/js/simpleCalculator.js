class SimpleCalculator {
	constructor () {
		this.total = 0;
	}
	add (x) {
		return this.total += x;
	}
	subtract (x) {
		return this.total -= x;
	}
	multiply (x) {
		return this.total *= x;
	}
	divide (x) {
		if (x === 0) {
			throw new Error('Cannot divide by 0');
		}
		return this.total /= x;
	}
}

Object.defineProperty(SimpleCalculator.prototype, 'version', {
	get: function() {
		return fetch('https://gist.githubusercontent.com/juanlizarazo/4b2d229ba483ca13b1a6d7bf3079dc8b/raw/228ac05e04118037be02c38d9b86945c1356a2e2/version.json')
			.then(function(result) {
				return result.json();
			})
			.then(function(json) {
				return json.version;
			});
	},
	enumerable: true,
	configurable: true
});